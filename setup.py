from setuptools import setup, find_packages

setup(
    name='model-adwords',
    version='0.11',
    description='Meltano .m5o models for Google Ads data fetched using the AdWords API',
    packages=find_packages(),
    package_data={'models': ['**/*.m5o', '*.m5o']},
    install_requires=[],
)
