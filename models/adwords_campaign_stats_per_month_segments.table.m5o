{
  include "mixins/month_start_timeframe.m5o"

  version = 1
  sql_table_name = adwords_campaign_stats_per_month_segments
  name = adwords_campaign_stats_per_month_segments
  columns {
    label {
      label = Report Label
      description = Report Label
      required = true
      type = string
      sql = "{{table}}.label"
    }
    month {
      label = Month
      description = Month
      type = string
      sql = "{{table}}.month"
    }
    month_start {
      label = Month Start Date
      type = date
      sql = "{{table}}.month_start"
    }
    network {
      label = Network
      description = Network
      type = string
      sql = "{{table}}.network"
    }
    device {
      label = Device
      description = Device
      type = string
      sql = "{{table}}.device"
    }
    account_name {
      label = Account Name
      description = Account Name
      type = string
      sql = "{{table}}.account_name"
    }
    campaign_name {
      label = Campaign Name
      description = Campaign Name
      type = string
      sql = "{{table}}.campaign_name"
    }
    currency {
      label = Account Currency
      description = Account Currency
      type = string
      sql = "{{table}}.currency"
    }
  }
  aggregates {
    impressions {
      label = Impressions
      description = "The number of times your ads have appeared on a search results page or website on the Google Network."
      type = sum
      sql = "{{table}}.impressions"
    }
    interactions {
      label = Interactions
      description = "The number of interactions."
      type = sum
      sql = "{{table}}.interactions"
    }
    interaction_rate {
      label = Interaction Rate (%)
      description = "Interaction Rate = (interactions / impressions) * 100%"
      type = avg
      sql = "{{table}}.interaction_rate"
    }
    cost {
      label = Cost
      description = "The sum of your cost-per-click (CPC) and cost-per-thousand impressions (CPM) costs during this period."
      type = sum
      sql = "{{table}}.cost"
    }
    clicks {
      label = Clicks
      description = "The number of clicks on your ads"
      type = sum
      sql = "{{table}}.clicks"
    }
    cpc {
      label = Cost Per Click
      description = "The average cost for each click"
      type = avg
      sql = "{{table}}.cpc"
    }
    cpm {
      label = CPM (Cost Per Mille)
      description = "The average cost for 1,000 impressions"
      type = avg
      sql = "{{table}}.cpm"
    }
    ctr {
      label = Click Through Rate (%)
      description = "The percentage of times people saw your ad and performed a click"
      type = avg
      sql = "{{table}}.ctr"
    }
    conversions {
      label = Conversions
      description = "The number of conversions for all conversion actions that you have opted into optimization."
      type = sum
      sql = "{{table}}.conversions"
    }
    cost_per_conversion {
      label = Cost Per Conversion
      description = "Cost per Convension = cost / conversions"
      type = avg
      sql = "{{table}}.cost_per_conversion"
    }
    conversion_rate {
      label = Conversion Rate (%)
      description = "Convension Rate = conversions / interactions * 100%"
      type = avg
      sql = "{{table}}.conversion_rate"
    }
  }
}
