{
  include "mixins/report_date_timeframe.m5o"

  version = 1
  sql_table_name = adwords_ad_report
  name = adwords_ad_report
  columns {
    account_id {
      label = Account ID
      hidden = true
      type = string
      sql = "{{table}}.account_id"
    }
    campaign_id {
      label = Campaign ID
      hidden = yes
      type = string
      sql = "{{TABLE}}.campaign_id"
    }
    ad_group_id {
      label = AdGroup ID
      hidden = true
      type = string
      sql = "{{table}}.ad_group_id"
    }

    report_date {
      label = Report Date
      type = date
      sql = "{{table}}.report_date"
    }
    network {
      label = Network
      description = Network
      type = string
      sql = "{{table}}.network"
    }
    device {
      label = Device
      description = Device
      type = string
      sql = "{{table}}.device"
    }

    account_name {
      label = Account Name
      description = Account Name
      type = string
      sql = "{{table}}.account_name"
    }
    currency {
      label = Account Currency
      description = Account Currency
      type = string
      sql = "{{table}}.currency"
    }
    account_time_zone {
      label = Account Time Zone
      description = Account Time Zone
      type = string
      sql = "{{table}}.account_time_zone"
    }
    campaign_name {
      label = Campaign Name
      description = Campaign Name
      type = string
      sql = "{{table}}.campaign_name"
    }
    campaign_status {
      label = Campaign Status
      description = Campaign Status
      type = string
      sql = "{{table}}.campaign_status"
    }
    ad_group_name {
      label = AdGroup Name
      description = AdGroup Name
      type = string
      sql = "{{table}}.ad_group_name"
    }
    ad_group_status {
      label = AdGroup Status
      description = AdGroup Status
      type = string
      sql = "{{table}}.ad_group_status"
    }

    ad_id {
      label = Ad ID
      type = string
      sql = "{{table}}.ad_id"
    }
    ad_type {
      label = Add Type
      description = Add Type
      type = string
      sql = "{{table}}.ad_type"
    }
    ad_status {
      label = Ad Status
      description = Ad Status
      type = string
      sql = "{{table}}.ad_status"
    }
    approval_status {
      label = Approval Status
      description = Approval Status
      type = string
      sql = "{{table}}.approval_status"
    }
    ad_strength {
      label = Add Strength
      description = Add Strength
      type = string
      sql = "{{table}}.ad_strength"
    }
  }
  aggregates {
    impressions {
      label = Impressions
      description = "The number of times your ads have appeared on a search results page or website on the Google Network."
      type = sum
      sql = "{{table}}.impressions"
    }
    interactions {
      label = Interactions
      description = "The number of interactions."
      type = sum
      sql = "{{table}}.interactions"
    }
    cost {
      label = Cost
      description = "The sum of your cost-per-click (CPC) and cost-per-thousand impressions (CPM) costs during this period."
      type = sum
      sql = "{{table}}.cost"
    }
    clicks {
      label = Clicks
      description = "The number of clicks on your ads"
      type = sum
      sql = "{{table}}.clicks"
    }
    conversions {
      label = Conversions
      description = "The number of conversions for all conversion actions that you have opted into optimization."
      type = sum
      sql = "{{table}}.conversions"
    }
    engagements {
      label = Engagements
      description = "The number of engagements. An engagement occurs when a viewer expands your Lightbox ad."
      type = sum
      sql = "{{table}}.engagements"
    }

    all_conversions {
      label = "All Conversions"
      description = "All Conversions"
      type = sum
      sql = "{{table}}.all_conversions"
    }
    all_conv_value {
      label = "All Conversion Value"
      description = "The total value of all of your conversions"
      type = sum
      sql = "{{table}}.all_conv_value"
    }
    video_views {
      label = "Video Views"
      description = "The number of times your video ads were viewed."
      type = sum
      sql = "{{table}}.video_views"
    }
    gmail_clicks_to_website {
      label = "Gmail Clicks to Website"
      description = "The number of clicks to your landing page on the expanded state of Gmail ads."
      type = sum
      sql = "{{table}}.gmail_clicks_to_website"
    }
    gmail_saves {
      label = "Gmail Saves"
      description = "The number of times someone has saved your Gmail ad to their inbox as a message."
      type = sum
      sql = "{{table}}.gmail_saves"
    }
    gmail_forwards {
      label = "Gmail Forwards"
      description = "The number of times your ad was forwarded to someone else as a message."
      type = sum
      sql = "{{table}}.gmail_forwards"
    }
    active_view_viewable_impressions {
      label = "Active View Impressions"
      description = "How often your ad has become viewable on a Display Network site."
      type = sum
      sql = "{{table}}.active_view_viewable_impressions"
    }
    active_view_measurable_cost {
      label = "Active View Measurable Cost"
      description = "The cost of the impressions you received that were measurable by Active View."
      type = sum
      sql = "{{table}}.active_view_measurable_cost"
    }
    active_view_measurable_impr_impr {
      label = "Active View Measurable Impressions"
      description = "The number of times your ads are appearing on placements in positions where they can be seen."
      type = sum
      sql = "{{table}}.active_view_measurable_impr_impr"
    }
    total_conv_value {
      label = "Total Conv. value"
      description = "The sum of conversion values for all conversions."
      type = sum
      sql = "{{table}}.total_conv_value"
    }
    conversions_current_model {
      label = "Current Model Attributed Conversions"
      description = "Current Model Attributed Conversions"
      type = sum
      sql = "{{table}}.conversions_current_model"
    }
    conv_value_current_model {
      label = "Current Model Attributed Conversion Value"
      description = "Current Model Attributed Conversion Value"
      type = sum
      sql = "{{table}}.conv_value_current_model"
    }
    cross_device_conv {
      label = "Cross Device Conversions"
      description = "Conversions from when a customer clicks on an ad on one device, then converts on a different device or browser."
      type = sum
      sql = "{{table}}.cross_device_conv"
    }
    view_through_conv {
      label = "View-through conversions"
      description = "The total number of view-through conversions."
      type = sum
      sql = "{{table}}.view_through_conv"
    }
  }
}
